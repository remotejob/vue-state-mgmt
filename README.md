# vue-state-mgmt

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

https://www.positronx.io/vue-js-vuex-state-management-tutorial-by-example/

https://github.com/SinghDigamber/vuex-state-management-app
