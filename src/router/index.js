// import { createRouter, createWebHashHistory } from 'vue-router
import { createRouter, createWebHistory } from 'vue-router'

// import { createRouter, createHashHistory } from 'vue-router'

// import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'User',
    component: () => import('../components/Users')
  }
]

const router = createRouter({
  history: createWebHistory('/'),
  // history: createWebHashHistory(),
  // mode: 'history',
  // base: process.env.BASE_URL,
  routes
})

export default router
