import { createStore } from 'vuex'
import UsersModule from '../store/modules/users-module'

export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    UsersModule
  }
})
